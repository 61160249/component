/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.component;

import java.util.ArrayList;

/**
 *
 * @author Triwit
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "gafair 1", 40, "01.jpg"));
        list.add(new Product(1, "gaf 2", 40, "02.jpg"));
        list.add(new Product(1, "gair 3", 40, "03.jpg"));
        list.add(new Product(1, "gafa 4", 40, "04.jpg"));
//        list.add(new Product(1, "gafairfa 1", 50, "01.jpg"));
//        list.add(new Product(1, "gafairfa 2", 50, "02.jpg"));
//        list.add(new Product(1, "gafairfa 3", 50, "03.jpg"));
//        list.add(new Product(1, "gafairga 1", 30, "04.jpg"));
//        list.add(new Product(1, "gafairga 2", 30, "03.jpg"));
        return list;
    }
}
